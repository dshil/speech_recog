package api

import (
	"bitbucket.org/speech_recog/api/controllers/audio"
	"github.com/gorilla/mux"
)

// Router contains all url routes and manages them
type Router struct {
	mux    *mux.Router
	routes []Route
}

// InitRoutes creates all url routes,
// also creates gorilla mux wrapper
func (r *Router) InitRoutes() {
	r.mux = mux.NewRouter().StrictSlash(true)

	// [name], [method], [path], [handler]
	route := Route{"ActionPost", "POST", "/", audio.ActionPost}
	r.routes = append(r.routes, route)

	r.setMux(r.routes)
}

func (r *Router) setMux(routes []Route) {
	for _, route := range routes {
		r.mux.
			Methods(route.method).
			Name(route.name).
			Path(route.path).
			Handler(route.handler)
	}
}

// Mux return gorilla mux router wrapper
func (r *Router) Mux() *mux.Router {
	return r.mux
}
