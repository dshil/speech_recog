package audio

import "net/http"

// ActionPost handel a query in which body audio will contain
// use Post because we can send a body using Get request
func ActionPost(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
}
