package api

import "net/http"

// Route represents a structure for url route
// handler is a pointer to the function that will call
// by mux in necessary time
type Route struct {
	name    string
	method  string
	path    string
	handler http.HandlerFunc
}
