package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

type server struct {
	port string
}

// NewServer creates new server with current port
func NewServer(port string) *server {
	return &server{port}
}

func (s *server) Start() {
	r := mux.NewRouter()
	http.ListenAndServe(":"+s.port, r)
}
