package api

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

// TestPostAction check if router initializes routes
// send base post request and expect 200 status code
// in confirmation that path exists
func TestPostAction(t *testing.T) {
	ts := TS()
	defer ts.Close()

	query := "/"

	res, err := http.Post(ts.URL+query, "application/json", nil)

	if err != nil {
		log.Println(http.StatusBadRequest)
	}

	expStatus := http.StatusCreated
	actStatus := res.StatusCode

	if actStatus != expStatus {
		msg := "Status code comparison failed, actual = %d, expected = %d"
		log.Fatalf(msg, actStatus, expStatus)
	}
}

// TS creates test server, also all existing routes create
func TS() *httptest.Server {
	r := new(Router)
	r.InitRoutes()

	return httptest.NewServer(r.Mux())
}
