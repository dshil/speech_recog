package main

import "bitbucket.org/speech_recog/api"

func main() {

	port := "8888"

	serv := api.NewServer(port)
	serv.Start()
}
